﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enums
{
    public enum Buttons
    {
        PreviousChannel,
        NextChannel
    };

    public enum LogType
    {
        Log,
        Warning,
        Error
    };
}

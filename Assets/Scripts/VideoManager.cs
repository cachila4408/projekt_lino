﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoManager : MonoBehaviour {

    public List <VideoClip> programs;

    GameManager gm;
    VideoPlayer vp;

    int maxPrograms;
    List<VideoClip> availablePrograms;
    int currentProgram = -1;

	void Awake ()
    {
        gm = FindObjectOfType<GameManager>();
        vp = GetComponent<VideoPlayer>();  
	}

    public int InitializeVideoManager (int playersNumber, int extraChannels)
    {
        if (CheckAvailability(playersNumber, extraChannels))
        {
            SetPrograms();
            ChangeChannel(1);
        }

        return maxPrograms;
    }

    // Check the availability of videoclips for the selected number of players and extra channels
    bool CheckAvailability (int playersNumber, int extraChannels)
    {
        Logger.Log("Check availability of programs", this.name, Enums.LogType.Log);

        bool isAvailable = false;
        int requiredPrograms = playersNumber + extraChannels;

        if (requiredPrograms <= programs.Count)
        {
            Logger.Log("Enough programs to show", this.name, Enums.LogType.Log);

            maxPrograms = requiredPrograms;
            isAvailable = true;
        }
        else if (playersNumber <= programs.Count)
        {
            Debug.LogWarning(this.name + ": Not enough programs to show extra channels");
            maxPrograms = Mathf.Min(requiredPrograms, programs.Count);
            isAvailable = true;
        }
        else
        {
            Debug.LogError(this.name + ": Not enough programs to show");
            maxPrograms = -1;
            isAvailable = false;
        }

        Debug.Log(this.name + ": maxPrograms = " + maxPrograms);

        return isAvailable;
    }

    void SetPrograms ()
    {
        Debug.Log(this.name + ": Set programs");

        availablePrograms = new List<VideoClip>();

        for ( int i = 0; i < maxPrograms; i++)
        {
            Debug.Log(this.name + ": Add " + (i+1) +" program");

            // TOCHANGE: Assegna un programma a ciascun canale
            availablePrograms.Add(programs[i]);
            currentProgram = -1;
        }
    }

    public void ChangeChannel (int delta)
    {
        Debug.Log("Try to change channel of " + delta);

        int nextProgram = currentProgram += delta;

        if (nextProgram < 0)
        {
            nextProgram = maxPrograms + nextProgram;
        }
        else if (nextProgram >= maxPrograms)
        {
            nextProgram = nextProgram - maxPrograms;
        }

        Debug.Log("Change channel to " + nextProgram);

        vp.clip = availablePrograms[nextProgram];
        vp.Play();
        currentProgram = nextProgram;

        gm.HandleProgramChanged(currentProgram);
    }
}

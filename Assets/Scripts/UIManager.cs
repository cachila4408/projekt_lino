﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public GameObject[] playerPanels;
    public Text[] playerScores;

    GameManager gm;

	void Start () {
        gm = FindObjectOfType<GameManager>();
	}

    public void InitializeUI (int playersNumber)
    {
        // Enable UI of active players
        for (int i = 0; i < playersNumber; i++)
        {
            playerPanels[i].SetActive(true);
        }
        // Disable UI of non active players
        for(int i = playersNumber; i < playerPanels.Length; i++)
        {
            playerPanels[i].SetActive(false);
        }
    }

    public void UpdatePlayerScore (int player, float score)
    {
        Logger.Log("Update player " + (player + 1) + " score to " + score, this.name, Enums.LogType.Log);
        playerScores[player].text = "Player " + (player + 1) + " " + score.ToString();
    }

    public void OnNextButtonPressed()
    {
        Debug.Log(this + ": Button next pressed");

        gm.HandleProgramButtonPressed(Enums.Buttons.NextChannel);
    }

    public void OnPreviousButtonPressed()
    {
        Debug.Log(this + ": Button previous pressed");

        gm.HandleProgramButtonPressed(Enums.Buttons.PreviousChannel);
    }
}

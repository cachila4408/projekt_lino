﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public int playersNumber = 1;
    public int extraChannels = 0;

    ScoreManager sm;
    UIManager uim;
    VideoManager vm;

    int maxPrograms;
    List<int> playersPrograms = new List<int> ();

    void Start () {
        sm = FindObjectOfType<ScoreManager>();
        uim = FindObjectOfType<UIManager>();
        vm = FindObjectOfType<VideoManager>();

        maxPrograms = vm.InitializeVideoManager(playersNumber, extraChannels);

        if (maxPrograms < 0)
        {
            Logger.Log("Unable to start the game", this.name, Enums.LogType.Error);
        }

        Debug.Log(this + "Starting the game");

        for (int i = 0; i < playersNumber; i++)
        {
            // TOCHANGE: Assegna un programma a ciascun giocatore
            playersPrograms.Add(i);
            Logger.Log("Player " + i + " program is " + i, this.name);
        }

        sm.InitializeScore(playersNumber);
        uim.InitializeUI(playersNumber);
	}

    public void HandleScoreUpdated (int scoringPlayer, float newScore)
    {
        Logger.Log("Call UI Manager to update player score label", this.name);
        uim.UpdatePlayerScore(scoringPlayer, newScore);
    }

    public void HandleProgramChanged (int newProgram)
    {
        int newScoringPlayer = -1;

        if (playersPrograms.Contains(newProgram))
        {
            newScoringPlayer = playersPrograms.IndexOf(newProgram);
        }

        Logger.Log("Call Score Manager to update scoring player to player " + newScoringPlayer, this.name, Enums.LogType.Log);
        sm.UpdateScoringPlayer(newScoringPlayer);
    }

    public void HandleProgramButtonPressed (Enums.Buttons buttonType)
    {
        switch (buttonType)
        {
            case Enums.Buttons.PreviousChannel:
                vm.ChangeChannel(-1);
                break;

            case Enums.Buttons.NextChannel:
                vm.ChangeChannel(1);
                break;
        }
    }
}

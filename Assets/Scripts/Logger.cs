﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Logger
{
    static string errorMessage = "Can't show message";

    public static void Log(string message, string name, Enums.LogType logType)
    {
        string prefix = name + ": ";
        string log = prefix + message;

        switch (logType)
        {
            case Enums.LogType.Log:
                Debug.Log(log);
                break;

            case Enums.LogType.Warning:
                Debug.LogWarning(log);
                break;

            case Enums.LogType.Error:
                Debug.LogError(log);
                break;

            default:
                Logger.Log(errorMessage, name, Enums.LogType.Error);
                break;
        }
    }

    public static void Log(string message, string name)
    {
        string prefix = name + ": ";
        string log = prefix + message;
        Debug.Log(log);
    }

    public static void Log(string message)
    {
        Debug.Log(message);
    }
}

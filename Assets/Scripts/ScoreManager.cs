﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour {

    public float scoreWait = 1f;
    public float scoreMultiplier = 1f;

    GameManager gm;
    List<float> playerScores = new List<float> ();
    int scoringPlayer = -1;

    private void Awake()
    {
        gm = FindObjectOfType<GameManager>(); 
    }

    public void InitializeScore (int playerNumber)
    {
        for (int i = 0; i < playerNumber; i++)
        {
            playerScores.Add(0);
        }
    }

    public void UpdateScoringPlayer (int newScoringPlayer)
    {
        Logger.Log("Player " + scoringPlayer + " stops scoring", this.name, Enums.LogType.Log);
        StopCoroutine("WaitAndScore");

        if (newScoringPlayer >= 0)
        {
            Logger.Log("Player " + newScoringPlayer + " starts scoring", this.name, Enums.LogType.Log);
            scoringPlayer = newScoringPlayer;
            StartCoroutine("WaitAndScore");
        }
    }

    IEnumerator WaitAndScore()
    {
        Logger.Log("Wait and score");
        yield return new WaitForSeconds(scoreWait);
        playerScores[scoringPlayer] += scoreMultiplier;
        gm.HandleScoreUpdated(scoringPlayer, playerScores[scoringPlayer]);
        WaitAndScore();
    }
}
